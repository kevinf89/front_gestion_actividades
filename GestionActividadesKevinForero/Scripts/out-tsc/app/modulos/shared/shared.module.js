import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertaComponent } from './alerta/alerta.component';
import { FooterComponent } from './footer/footer.component';
import { TopBarComponent } from './top-bar/top-bar.component';
let ShareddModule = class ShareddModule {
};
ShareddModule = tslib_1.__decorate([
    NgModule({
        declarations: [AlertaComponent, FooterComponent, TopBarComponent],
        imports: [
            CommonModule
        ], exports: [TopBarComponent, FooterComponent, AlertaComponent]
    })
], ShareddModule);
export { ShareddModule };
//# sourceMappingURL=shared.module.js.map