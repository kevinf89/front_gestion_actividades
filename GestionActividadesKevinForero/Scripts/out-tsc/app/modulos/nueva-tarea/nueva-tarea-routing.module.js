import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NuevaTareaComponent } from './nueva-tarea/nueva-tarea.component';
const EC_ROUTES = [
    { path: ':idModulo', component: NuevaTareaComponent }
];
let NuevaTareaRoutingModule = class NuevaTareaRoutingModule {
};
NuevaTareaRoutingModule = tslib_1.__decorate([
    NgModule({
        declarations: [],
        imports: [
            RouterModule.forChild(EC_ROUTES)
        ],
        exports: [
            RouterModule
        ]
    })
], NuevaTareaRoutingModule);
export { NuevaTareaRoutingModule };
//# sourceMappingURL=nueva-tarea-routing.module.js.map