import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NuevaTareaRoutingModule } from './nueva-tarea-routing.module';
import { NuevaTareaComponent } from './nueva-tarea/nueva-tarea.component';
let NuevaTareaModule = class NuevaTareaModule {
};
NuevaTareaModule = tslib_1.__decorate([
    NgModule({
        declarations: [NuevaTareaComponent],
        imports: [
            CommonModule, NuevaTareaRoutingModule,
            ReactiveFormsModule, FormsModule
        ]
    })
], NuevaTareaModule);
export { NuevaTareaModule };
//# sourceMappingURL=nueva-tarea.module.js.map