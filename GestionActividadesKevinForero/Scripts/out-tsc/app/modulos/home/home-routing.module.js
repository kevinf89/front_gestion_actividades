import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
const HOME_ROUTES = [
    { path: '', component: HomeComponent },
    { path: 'NuevaTarea', loadChildren: () => import('../nueva-tarea/nueva-tarea.module').then(mod => mod.NuevaTareaModule) }
];
let HomeRoutingModule = class HomeRoutingModule {
};
HomeRoutingModule = tslib_1.__decorate([
    NgModule({
        imports: [
            RouterModule.forChild(HOME_ROUTES)
        ],
        exports: [
            RouterModule
        ], providers: [],
    })
], HomeRoutingModule);
export { HomeRoutingModule };
//# sourceMappingURL=home-routing.module.js.map