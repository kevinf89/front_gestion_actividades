import * as tslib_1 from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgModule, APP_INITIALIZER, LOCALE_ID } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ShareddModule } from './modulos/shared/shared.module';
import { HomeModule } from './modulos/home/home.module';
//import { LayoutModule } from '@progress/kendo-angular-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { ErrorInterceptor } from './Interceptador/error.interceptor';
//registerLocaleData(localeEs,'es-ES');
// export function iniciar(http: HttpClient,usuarioService: UsuarioService, globalService: GlobalService): (() => Promise<boolean>) {
export function iniciar(http) {
    return () => {
        return new Promise((resolve) => {
            let config = {};
            const jsonFile = 'assets/config.json';
            http.get(jsonFile).toPromise().then((ConfigJson) => {
                config.urlGestion = ConfigJson.urlGestion;
                // config
                // if(config.EjecutarLocal){
                //   config.Token = ConfigJson.Token; 
                // }
                sessionStorage.setItem('config', JSON.stringify(config));
                resolve(true);
            });
        });
    };
}
let AppModule = class AppModule {
};
AppModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            AppComponent
        ],
        imports: [
            BrowserModule,
            BrowserAnimationsModule,
            AppRoutingModule,
            HttpClientModule,
            ShareddModule,
            HomeModule
            // LayoutModule,
            // DropDownsModule
        ],
        providers: [
            {
                provide: APP_INITIALIZER,
                useFactory: iniciar,
                deps: [
                    HttpClient
                ],
                multi: true
            },
            { provide: LOCALE_ID, useValue: 'es-ES' },
        ],
        bootstrap: [AppComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map