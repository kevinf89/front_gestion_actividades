import { NgModule, InjectionToken }             from '@angular/core';
import { RouterModule, Routes, ActivatedRouteSnapshot } from '@angular/router';
import { HomeComponent } from './home/home.component';

const HOME_ROUTES: Routes = [
  { path: '', component: HomeComponent},
  { path: 'NuevaTarea', loadChildren: () => import('../nueva-tarea/nueva-tarea.module').then(mod => mod.NuevaTareaModule) }

];
@NgModule({
  imports: [
    RouterModule.forChild(HOME_ROUTES)
  ],
  exports: [
    RouterModule
  ], providers: [
    
],
})
export class HomeRoutingModule { }
