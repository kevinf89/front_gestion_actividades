import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule   } from '@angular/forms';

import { NuevaTareaRoutingModule } from './nueva-tarea-routing.module';

import { NuevaTareaComponent } from './nueva-tarea/nueva-tarea.component';

@NgModule({
  declarations: [NuevaTareaComponent],
  imports: [
    CommonModule,NuevaTareaRoutingModule,
    ReactiveFormsModule,FormsModule
  ]
})




export class NuevaTareaModule { }
