import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {NuevaTareaComponent} from './nueva-tarea/nueva-tarea.component'

const EC_ROUTES: Routes = [
  { path: ':idModulo', component: NuevaTareaComponent}]

  @NgModule({
    declarations: [],
    imports: [
      RouterModule.forChild(EC_ROUTES)
    ],
    exports: [
      RouterModule 
    ]
  })
export class NuevaTareaRoutingModule { }
