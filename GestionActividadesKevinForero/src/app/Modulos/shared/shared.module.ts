import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertaComponent } from './alerta/alerta.component';
import { FooterComponent } from './footer/footer.component';
import { TopBarComponent } from './top-bar/top-bar.component';

@NgModule({
  declarations: [AlertaComponent, FooterComponent, TopBarComponent],
  imports: [
    CommonModule
  ],exports: [TopBarComponent,FooterComponent,AlertaComponent]
})
export class ShareddModule { }
